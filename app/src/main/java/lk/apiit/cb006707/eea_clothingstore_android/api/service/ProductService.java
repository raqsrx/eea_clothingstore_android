package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import java.util.List;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Product;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ProductService {
    @GET("products/")
    Call<List<Product>> getAllProducts();
}
