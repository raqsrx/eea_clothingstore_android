package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import java.util.List;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Discount;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DiscountService {
    @GET("discounts/")
    Call<List<Discount>> getAllDiscounts();

    @GET("discounts/{discountId")
    Call<Discount> getDiscountById(@Path("discountId") String discountId);
}
