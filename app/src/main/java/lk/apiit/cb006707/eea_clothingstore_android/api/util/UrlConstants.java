package lk.apiit.cb006707.eea_clothingstore_android.api.util;

public class UrlConstants {
    private static final String HOST = "http://localhost:";
    private static final String PORT = "8082";
    public static final String BASE_URL = HOST + PORT + "/";
}
