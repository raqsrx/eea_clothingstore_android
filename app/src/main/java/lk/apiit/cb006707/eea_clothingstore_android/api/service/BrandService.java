package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Brand;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BrandService {
    @GET("brands/")
    Call<Brand> getAllBrands();
}
