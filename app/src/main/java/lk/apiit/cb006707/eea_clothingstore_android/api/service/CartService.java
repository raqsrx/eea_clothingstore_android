package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Cart;
import lk.apiit.cb006707.eea_clothingstore_android.api.model.CartItem;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CartService {
    @GET("carts/users/{userId}")
    Call<Cart> getUserCart(@Path("userId") String userId);

    @POST("carts/{cartId}/items/")
    Call<CartItem> addCartItem(@Path("cartId") String cartId, @Body CartItem cartItem);

    @PUT("carts/{cartId}/items/{itemId}")
    Call<CartItem> updateCartItem(@Path("cartId") String cartId, @Path("itemId") String itemId,
                                  @Body CartItem cartItem);

    @DELETE("carts/{cartId}/items/{itemId}")
    Call deleteCartItem(@Path("cartId") String cartId, @Path("itemId") String itemId);
}
