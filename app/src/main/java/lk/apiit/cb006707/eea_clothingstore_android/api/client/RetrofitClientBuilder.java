package lk.apiit.cb006707.eea_clothingstore_android.api.client;

import lk.apiit.cb006707.eea_clothingstore_android.api.service.BrandService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.CartService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.CategoryService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.DiscountService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.ProductService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.SaleService;
import lk.apiit.cb006707.eea_clothingstore_android.api.service.UserService;
import lk.apiit.cb006707.eea_clothingstore_android.api.util.UrlConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientBuilder {
    private static final RetrofitClientBuilder retrofitClientBuilder = new RetrofitClientBuilder();
    private Retrofit client;

    private RetrofitClientBuilder() {
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(chain -> {
//            Request request = chain.request()
//                    .newBuilder()
//                    .build();
//            return chain.proceed(request);
//        });
        this.client = new Retrofit
                .Builder()
                .baseUrl(UrlConstants.BASE_URL)
//                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitClientBuilder getInstance() {
        return retrofitClientBuilder;
    }

    public BrandService getBrandService() {
        return client.create(BrandService.class);
    }

    public CategoryService getCategoryService() {
        return client.create(CategoryService.class);
    }

    public CartService getCartService() {
        return client.create(CartService.class);
    }

    public DiscountService getDiscountService() {
        return client.create(DiscountService.class);
    }

    public ProductService getProductService() {
        return client.create(ProductService.class);
    }

    public SaleService getSaleService() {
        return client.create(SaleService.class);
    }

    public UserService getUserService() {
        return client.create(UserService.class);
    }
}
