package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import java.util.List;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Address;
import lk.apiit.cb006707.eea_clothingstore_android.api.model.CreditCard;
import lk.apiit.cb006707.eea_clothingstore_android.api.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @POST("users/")
    Call<User> registerUser(@Body User user);

    @GET("users/{userId}/addresses")
    Call<List<Address>> getAllAddresses(@Path("userId") String userId);

    @POST("users/{userId}/addresses")
    Call<Address> createAddress(@Path("userId") String userId, @Body Address address);

    @PUT("users/{userId}/addresses/{addressId}")
    Call updateAddress(@Path("userId") String userId, @Path("addressId") String addressId,
                       @Body Address address);

    @DELETE("users/{userId}/addresses/{addressId}")
    Call deleteAddress(@Path("userId") String userId, @Path("addressId") String addressId);

    @GET("users/{userId}/cards")
    Call<List<CreditCard>> getAllCards(@Path("userId") String userId);

    @POST("users/{userId}/cards")
    Call<CreditCard> createCard(@Path("userId") String userId, @Body CreditCard creditCard);

    @PUT("users/{userId}/cards/{cardId}")
    Call deleteCard(@Path("userId") String userId, @Path("cardId") String cardId,
                    @Body CreditCard creditCard);

    @DELETE("users/{userId}/cards/{cardId}")
    Call deleteCard(@Path("userId") String userId, @Path("cardId") String cardId);

}
