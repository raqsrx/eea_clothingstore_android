package lk.apiit.cb006707.eea_clothingstore_android.api.service;

import java.util.List;

import lk.apiit.cb006707.eea_clothingstore_android.api.model.Sale;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SaleService {
    @GET("sales/")
    Call<List<Sale>> getAllSales();

    @POST("sales/")
    Call<Sale> create(@Body Sale sale);
}
